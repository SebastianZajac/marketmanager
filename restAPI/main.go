package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/marketManager/core/model"
	"github.com/marketManager/core/patternRunner"
)

type Article struct {
	Title   string `json:"Title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
}

type Articles []Article

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func returnAllArticles(w http.ResponseWriter, r *http.Request) {
	articles := Articles{
		Article{Title: "Hello", Desc: "Article Description", Content: "Article Content"},
		Article{Title: "Hello 2", Desc: "Article Description", Content: "Article Content"},
	}
	fmt.Println("Endpoint Hit: returnAllArticles")

	json.NewEncoder(w).Encode(articles)
}

func returnOneArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["key"]
	var1 := vars["var1"]
	var2 := vars["var2"]

	fmt.Println("Var 1: " + var1)
	fmt.Println("Var 2: " + var2)
	fmt.Fprintf(w, "Key: "+key)
}

type filterParams struct {
	DateStart   int
	DateEnd     int
	PatternType string
}

func returnPattern(w http.ResponseWriter, r *http.Request) {
	// vars := mux.Vars(r)
	params := r.URL.Query()
	fmt.Println("Endpoint Hit: /pattern")

	if len(params["types"]) > 0 {
		patternType := params["types"][0]

		fmt.Println("Type = " + patternType)
	}
	if len(params["timeStart"]) > 0 {
		dateStart := params["timeStart"][0]

		fmt.Println("Timestart = " + dateStart)
	}
	if len(params["timeEnd"]) > 0 {
		dateEnd := params["timeEnd"][0]

		fmt.Println("Timeend = " + dateEnd)
	}

	candleCollections := patternRunner.SetUp()
	result := [][]model.Candle{}
	for _, collection := range candleCollections {
		result = append(result, collection)
	}
	json.NewEncoder(w).Encode(result)
}

func handleRequests() {
	fmt.Println("/pattern?types=X&dateStart=X&dateEnd=X GET")
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/all", returnAllArticles)
	myRouter.HandleFunc("/article/{key}", returnOneArticle)
	myRouter.HandleFunc("/pattern", returnPattern)
	log.Fatal(http.ListenAndServe(":10000", handlers.CORS()(myRouter)))
}

func main() {
	fmt.Println("Market Manager API v0.1")
	handleRequests()
}
