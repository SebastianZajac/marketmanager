package patternFactory

import (
	"errors"

	"github.com/marketManager/core/model"
	"github.com/marketManager/pattern"
)

type Pattern interface {
	Match(*model.Product) map[int]model.Candle
}

const (
	HAMMER = iota
	FALLING_STAR
)

func CreatePattern(t int) (Pattern, error) {
	switch t {
	case HAMMER:
		hammer := new(pattern.Hammer)
		hammer.SetDefault()
		return hammer, nil
	case FALLING_STAR:
		// return new(pattern.FallingStar), nil
		return nil, errors.New("invalid Type2")
	default:
		return nil, errors.New("invalid Type")
	}
}
