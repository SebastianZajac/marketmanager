package pattern

import (
	"math"

	"github.com/marketManager/core/model"
)

type Hammer struct {
	eurusd1pip             float64
	lowerShadowToUpperProp float64
	bodyToLowerShadowProp  float64
	minimumBodySize        float64
}

func (this *Hammer) SetDefault() {
	this.eurusd1pip = 0.00001
	this.lowerShadowToUpperProp = 3
	this.bodyToLowerShadowProp = 1.3
	this.minimumBodySize = this.eurusd1pip * 3
}

func (this *Hammer) Match(p *model.Product) map[int]model.Candle {
	candles := make(map[int]model.Candle)
	for i, candle := range p.Candles {
		if this.rule(candle) {
			candles[i] = candle
		}
	}

	return candles
}

func (this *Hammer) rule(c model.Candle) bool {
	return this.lowerShadowToUpperShadowProportions(c) &&
		this.isRealBodyLongEnough(c) &&
		this.bodyToLowerShadowProportions(c)
}

func (this *Hammer) bodyToLowerShadowProportions(c model.Candle) bool {
	realBodyHeight := this.getRealBodyHeight(c)
	if c.Open > c.Close {
		return ((c.Close - c.Low) / realBodyHeight) >= this.bodyToLowerShadowProp
	}

	return ((c.Open - c.Low) / realBodyHeight) >= this.bodyToLowerShadowProp
}

func (this *Hammer) lowerShadowToUpperShadowProportions(c model.Candle) bool {
	if c.Open > c.Close {
		return ((c.Close - c.Low) / (c.High - c.Open)) >= this.lowerShadowToUpperProp
	}

	return ((c.Open - c.Low) / (c.High - c.Close)) >= this.lowerShadowToUpperProp
}

func (this *Hammer) isRealBodyLongEnough(c model.Candle) bool {
	realBodyHeight := this.getRealBodyHeight(c)

	return realBodyHeight >= this.minimumBodySize
}

func (this *Hammer) getRealBodyHeight(c model.Candle) float64 {
	return math.Abs(c.Open - c.Close)
}
