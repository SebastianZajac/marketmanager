package patternRunner

import (
	"fmt"

	"github.com/marketManager/core/model"
	"github.com/marketManager/filler"
	"github.com/marketManager/filter"
	"github.com/marketManager/loader"
	"github.com/marketManager/patternFactory"
)

func SetUp() map[int][]model.Candle {
	p := setUpModels()
	loader.LoadCSV(p)
	fmt.Println(len(p.Candles))
	var myType int
	myType = 0
	pattern, err := patternFactory.CreatePattern(myType)
	if err == nil {
		candles := pattern.Match(p)
		resultWithLowest := filter.LocalLow(p, candles)
		result := filler.Fill(p, resultWithLowest)
		fmt.Printf("%d: FS pattern, only local high(10 candles before the match)\n", len(result))
		return result
	} else {
		fmt.Println(err)
	}

	return nil
	// fmt.Printf("%d: FS pattern matched number\n", len(candles))

	// candles := filter.Hammer(p)
	// fmt.Printf("%d: FS pattern matched number\n", len(candles))
	// result := filter.LocalLow(p, candles)

}

func setUpModels() *model.Product {
	m := model.Market{Name: "EURUSD"}
	i := model.Interval{Name: "15M"}
	p := new(model.Product)
	p.SetMarket(m)
	p.SetInterval(i)

	return p
}
