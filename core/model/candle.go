package model

type Candle struct {
	Timestamp int64   `json:"x,omitempty"`
	High      float64 `json:"high,omitempty"`
	Open      float64 `json:"open,omitempty"`
	Close     float64 `json:"close,omitempty"`
	Low       float64 `json:"low,omitempty"`
}

func (this *Candle) SetTimestamp(d int64) {
	this.Timestamp = d
}

func (this *Candle) SetHigh(value float64) {
	this.High = value
}

func (this *Candle) SetOpen(value float64) {
	this.Open = value
}

func (this *Candle) SetClose(value float64) {
	this.Close = value
}

func (this *Candle) SetLow(value float64) {
	this.Low = value
}
