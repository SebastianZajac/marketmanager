package model

import "gopkg.in/mgo.v2/bson"

type Product struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Market   Market
	Interval Interval
	Candles  []Candle `bson:"candles"`
}

func (this *Product) SetMarket(m Market) {
	this.Market = m
}

func (this *Product) SetInterval(i Interval) {
	this.Interval = i
}

func (this *Product) AddCandle(c Candle) {
	this.Candles = append(this.Candles, c)
}
