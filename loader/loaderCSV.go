package loader

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/marketManager/core/model"
)

func LoadCSV(p *model.Product) {
	loadFile(p)
}

func loadFile(p *model.Product) {

	csvfile1, err := os.Open("../data/EURUSD_Candlestick_1_m_BID_01.05.2014-28.05.2016.csv")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer csvfile1.Close()
	reader := csv.NewReader(csvfile1)

	records, _ := reader.ReadAll()
	for i, row := range records {
		if i == 0 {
			continue
		}
		c := new(model.Candle)
		high, _ := strconv.ParseFloat(row[2], 64)
		open, _ := strconv.ParseFloat(row[1], 64)
		close, _ := strconv.ParseFloat(row[4], 64)
		low, _ := strconv.ParseFloat(row[3], 64)
		t, _ := time.Parse("02.01.2006 15:04:05.999", row[0])
		milis := t.Unix() * 1000
		c.SetTimestamp(milis)
		c.SetHigh(high)
		c.SetOpen(open)
		c.SetClose(close)
		c.SetLow(low)
		p.AddCandle(*c)
	}
}
