package filler

import "github.com/marketManager/core/model"

const numberOfCandles int = 10

func Fill(p *model.Product, candles map[int]model.Candle) map[int][]model.Candle {
	t := make(map[int][]model.Candle)
	for i, _ := range candles {

		if i-numberOfCandles > 0 && (len(p.Candles)-i+numberOfCandles) > 0 {

			sample := p.Candles[i-numberOfCandles : i+numberOfCandles]
			t[i] = sample
		}
	}

	return t
}
