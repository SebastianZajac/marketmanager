package filter

import "github.com/marketManager/core/model"

func FallingStar(p *model.Product) map[int]model.Candle {
	result := make(map[int]model.Candle)
	for i, candle := range p.Candles {
		if rule(candle) {
			result[i] = candle
		}
	}

	return result
}

func rule(c model.Candle) bool {

	if c.Open > c.Close &&
		c.High > c.Open &&
		(c.Low == c.Close || (c.Close > c.Low && ((c.Open - c.Close) > (c.Open-c.Low)/1.5))) &&
		2*(c.Open-c.Close) < (c.High-c.Close) {
		return true
	}
	return false
}
