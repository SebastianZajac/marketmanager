package filter

import (
	"github.com/marketManager/core/model"
)

const numberOfCandles int = 10

func LocalLow(p *model.Product, candles map[int]model.Candle) map[int]model.Candle {
	t := make(map[int]model.Candle)
	for i, candle := range candles {
		result := true
		if i-numberOfCandles > 0 {

			sample := p.Candles[i-numberOfCandles : i]
			for _, s := range sample {
				result = result && s.Close >= candle.Open
			}
			if result {
				t[i] = candle
			}
		}
	}

	return t
}
